const express = require('express');

const app = express();
const port = 3000;
app.use(express.json())
app.use(express.urlencoded({extended:true}));




//=============Home====================
app.get("/home", (req, res) => {
    //res.send uses the express JS module's method to send a response back to the client
    res.send("Welcome to the homepage");
    
});


//=================USERS===========================
let users = [];

 app.get("/users", (req, res) => {
    console.log(req.body);
   
    if(req.body.username !== '' && req.body.password !== ''){
       
        users.push(req.body);
        res.send(users);

    }else {
         res.send("Please input BOTH username and password!");
    }
    
 });


//===================DELETE=======================
app.delete("/delete-user", (req, res) => {
    console.log(req.body);
   
    if(req.body.username !== ''){
       
        users.pop(req.body);//.pull if delete
        res.send(`User ${req.body.username} has been deleted`);

    }else {
         res.send("Please input user to be deleted!");
    }
 });



app.listen(port, () => console.log(`Server Running at port ${port}`))

